const { assert } = require('chai');
const sinon = require('sinon');

const { sudokuCheckerService } = require('../services');

// Dummy constant for test
const correctAnswer = [
  [5, 3, 2, 4, 8, 7, 6, 9, 1],
  [9, 8, 6, 2, 1, 5, 4, 3, 7],
  [7, 4, 1, 3, 6, 9, 5, 8, 2],
  [3, 2, 5, 7, 6, 4, 8, 1, 9],
  [1, 7, 4, 3, 9, 8, 5, 6, 2],
  [8, 9, 6, 1, 2, 5, 4, 3, 7],
  [1, 5, 6, 9, 7, 8, 2, 4, 3],
  [8, 2, 3, 6, 4, 1, 7, 5, 9],
  [9, 7, 4, 2, 5, 3, 6, 1, 8],
];

const blockIncorrectAnswer = [
  [5, 5, 5, 3, 8, 7, 6, 9, 1],
  [9, 8, 2, 2, 1, 5, 4, 3, 7],
  [7, 4, 1, 3, 6, 9, 5, 8, 2],
  [3, 2, 5, 7, 6, 4, 8, 1, 9],
  [1, 7, 4, 3, 9, 8, 5, 6, 2],
  [8, 9, 6, 1, 2, 5, 4, 3, 7],
  [1, 5, 6, 9, 7, 8, 2, 4, 3],
  [8, 2, 3, 6, 4, 1, 7, 5, 9],
  [9, 7, 4, 2, 5, 3, 6, 1, 8],
];

const verticalIncorrectAnswer = [
  [5, 3, 2, 5, 8, 7, 6, 9, 1],
  [9, 8, 2, 2, 1, 5, 4, 3, 7],
  [7, 4, 1, 3, 6, 9, 5, 8, 2],
  [3, 2, 5, 7, 6, 4, 8, 1, 9],
  [1, 7, 4, 3, 9, 8, 5, 6, 2],
  [8, 9, 6, 1, 2, 5, 4, 3, 7],
  [1, 5, 6, 9, 7, 8, 2, 4, 3],
  [8, 2, 3, 6, 4, 1, 7, 5, 9],
  [9, 7, 4, 2, 5, 3, 6, 1, 8],
];

const horizontalIncorrectAnswer = [
  [5, 3, 2, 4, 8, 7, 6, 9, 1],
  [9, 8, 2, 2, 1, 5, 4, 3, 7],
  [7, 4, 1, 3, 6, 9, 5, 8, 2],
  [3, 2, 5, 7, 6, 4, 8, 1, 9],
  [1, 7, 4, 3, 9, 8, 5, 6, 2],
  [8, 9, 6, 1, 2, 5, 4, 3, 7],
  [1, 5, 6, 9, 7, 8, 2, 4, 3],
  [8, 2, 3, 6, 4, 1, 7, 5, 9],
  [9, 7, 4, 2, 5, 3, 6, 1, 8],
];

// Tests scope
describe('sudokuCheckerService', () => {
  describe('checkAnswer', () => {
    it('should return true if checkVerticalDuplicate and checkHorizontalDuplicate return true', () => {
      sinon
        .stub(sudokuCheckerService, 'checkHorizontalDuplicate')
        .returns(true);
      sinon.stub(sudokuCheckerService, 'checkVerticalDuplicate').returns(true);
      assert.equal(
        sudokuCheckerService.checkAnswer([]),
        true,
        'checkAnswer is not returning the expected result, should return true'
      );
      sinon.restore();
    });
    it('should return false if one/both of checkVerticalDuplicate or checkHorizontalDuplicate return false', () => {
      // checkHorizontalDuplicate false ; checkVerticalDuplicate false
      sinon
        .stub(sudokuCheckerService, 'checkHorizontalDuplicate')
        .returns(false);
      sinon.stub(sudokuCheckerService, 'checkVerticalDuplicate').returns(false);
      assert.equal(
        sudokuCheckerService.checkAnswer([]),
        false,
        'checkAnswer is not returning the expected result, should return true'
      );
      sinon.restore();
      // checkHorizontalDuplicate false ; checkVerticalDuplicate true
      sinon
        .stub(sudokuCheckerService, 'checkHorizontalDuplicate')
        .returns(false);
      sinon.stub(sudokuCheckerService, 'checkVerticalDuplicate').returns(true);
      assert.equal(
        sudokuCheckerService.checkAnswer([]),
        false,
        'checkAnswer is not returning the expected result, should return true'
      );
      sinon.restore();
      // checkHorizontalDuplicate true ; checkVerticalDuplicate false
      sinon
        .stub(sudokuCheckerService, 'checkHorizontalDuplicate')
        .returns(true);
      sinon.stub(sudokuCheckerService, 'checkVerticalDuplicate').returns(false);
      assert.equal(
        sudokuCheckerService.checkAnswer([]),
        false,
        'checkAnswer is not returning the expected result, should return true'
      );
      sinon.restore();
    });
    // integration
    it('should return true for correct answer input', () => {
      assert.equal(
        sudokuCheckerService.checkAnswer(correctAnswer),
        true,
        'should return true'
      );
    });
  });
  describe('checkBlockDuplicate', () => {
    it('should return true for no duplicate found in block array', () => {
      assert.equal(
        sudokuCheckerService.checkBlockDuplicate(correctAnswer),
        true,
        'should return true'
      );
    });
    it('should return false for duplicate found in the block array', () => {
      assert.equal(
        sudokuCheckerService.checkBlockDuplicate(blockIncorrectAnswer),
        false,
        'should return false'
      );
    });
  });
  describe('checkHorizontalDuplicate', () => {
    it('should return true for no duplicate found in horizontal array', () => {
      assert.equal(
        sudokuCheckerService.checkHorizontalDuplicate(correctAnswer),
        true,
        'should return true'
      );
    });
    it('should return false for duplicate found in the horizontal array', () => {
      assert.equal(
        sudokuCheckerService.checkHorizontalDuplicate(
          horizontalIncorrectAnswer
        ),
        false,
        'should return false'
      );
    });
  });
  describe('checkVerticalDuplicate', () => {
    it('should return true for no duplicate found in vertical array', () => {
      assert.equal(
        sudokuCheckerService.checkVerticalDuplicate(correctAnswer),
        true,
        'should return true'
      );
    });
    it('should return false for duplicate found in the vertical array', () => {
      assert.equal(
        sudokuCheckerService.checkVerticalDuplicate(verticalIncorrectAnswer),
        false,
        'should return false'
      );
    });
  });
});
