const { sudokuCheckerService } = require('./services');

// sudoku answer input
let sudokuAnswer = [
  // array representing row left right, up down
  [5, 3, 2, 4, 8, 7, 6, 9, 1], // array representing block from left to right
  [9, 8, 6, 2, 1, 5, 4, 3, 7],
  [7, 4, 1, 3, 6, 9, 5, 8, 2],
  [3, 2, 5, 7, 6, 4, 8, 1, 9],
  [1, 7, 4, 3, 9, 8, 5, 6, 2],
  [8, 9, 6, 1, 2, 5, 4, 3, 7],
  [1, 5, 6, 9, 7, 8, 2, 4, 3],
  [8, 2, 3, 6, 4, 1, 7, 5, 9],
  [9, 7, 4, 2, 5, 3, 6, 1, 8],
];

let defaultInput = true;
let printHelp = false;

if (process.argv.length > 2) {
  process.argv.slice(2, process.argv.length).forEach(arg => {
    const completeArg = arg.split('=');
    if (completeArg[0] == '--sudoku') {
      sudokuAnswer = JSON.parse(completeArg[1]);
      defaultInput = false;
    }
  });
}

if (defaultInput) {
  console.log('--sudoku flag not added, using test answer as input...');
}

console.log('sudoku input:\n', sudokuAnswer);
// check sudoku answer
const result = sudokuCheckerService.checkAnswer(sudokuAnswer);
if (result) {
  console.log('The complete sudoku is correct');
} else {
  console.log('The sudoku is incorrect');
}
