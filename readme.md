### Sudoku Checker

Interview assignment to check sudoku correctness

- `sudoku answer input`: is the input of sudoku formated in the form of
  - 2D array -> `[[block_a], [block_b], [block_c], [block_d], [block_e], [block_f], [block_g], [block_h], [block_i]]`
  - graphically it's:
    ```
    [block_a] [block_b] [block_c]
    [block_d] [block_e] [block_f]
    [block_g] [block_h] [block_i]
    ```
  - for each block array:
    ```
    1, 2, 3
    4, 5, 6,
    7, 8, 9
    ```
    will be represented as a 1D array of `[1, 2, 3, 4, 5, 6, 7, 8, 9]`

### Install

- `npm install`

### Run

- `node index.js` / `npm run` to run using the default input, will return `correct`
- `node index.js --sudoku="[[5, 3, 2, 4, 8, 7, 6, 9, 1],[9, 8, 6, 2, 1, 5, 4, 3, 7],[7, 4, 1, 3, 6, 9, 5, 8, 2],[3, 2, 5, 7, 6, 4, 8, 1, 9],[1, 7, 4, 3, 9, 8, 5, 6, 2],[8, 9, 6, 1, 2, 5, 4, 3, 7],[1, 5, 6, 9, 7, 8, 2, 4, 3],[8, 2, 3, 6, 4, 1, 7, 5, 9],[9, 7, 4, 2, 5, 3, 6, 1, 8]]"` with the `--sudoku` as the `sudoku answer input` of 9x9 size to replace the default input.

### Run test

- `npm test`
