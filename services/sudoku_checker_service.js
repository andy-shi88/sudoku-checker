class SudokuCheckerService {
  /**
   * SudokuCheckerService class to encapsulate all sudoku checking logic
   */
  constructor() {
    console.log('SudokuCheckerService cosntructed');
  }

  hello() {
    console.log('hello there');
  }

  /**
   * checkAnswer to check the answer of (complete sudoku)
   * @param {Array[Array]} answer
   * @return {Boolean}
   */
  checkAnswer(answer) {
    const result =
      this.checkBlockDuplicate(answer) &&
      this.checkVerticalDuplicate(answer) &&
      this.checkHorizontalDuplicate(answer);
    return result;
  }

  /**
   * checkBlockDuplicate check for duplicate number in a block
   * @param {Array[Array]} answer
   * @return {Boolean}
   */
  checkBlockDuplicate(answer) {
    let result = true;
    for (let i = 0; i < answer.length; i++) {
      if (!this.checkDuplicate(answer[i])) {
        result = false;
        break;
      }
    }
    return result;
  }
  /**
   * checkVerticalDuplicate check for duplicate occurence in a column
   * @param {Array[Array]} answer
   * @return {Boolean}
   */
  checkVerticalDuplicate(answer) {
    let result = true;

    // normalize array to get vertical row
    for (let c = 0; c < 3; c++) {
      for (let i = 0; i < 3; i++) {
        const tempRow = [
          answer[c][i],
          answer[c][i + 3],
          answer[c][i + 6],
          answer[c + 3][i],
          answer[c + 3][i + 3],
          answer[c + 3][i + 6],
          answer[c + 6][i],
          answer[c + 6][i + 3],
          answer[c + 6][i + 6],
        ];
        // check for duplicate in the array built
        if (!this.checkDuplicate(tempRow)) {
          result = false;
          break;
        }
      }
    }
    return result;
  }

  /**
   * checkHorizontalDuplicate check for duplicate occurence in a row
   * @param {Array[Array]} answer
   * @return {Boolean}
   */
  checkHorizontalDuplicate(answer) {
    let result = true;
    // normalize array to get horizontal row
    for (let c = 0; c < answer.length; c += 3) {
      for (let i = 0; i < answer.length; i += 3) {
        const tempRow = [
          ...answer[i].slice(c, c + 3),
          ...answer[i + 1].slice(c, c + 3),
          ...answer[i + 2].slice(c, c + 3),
        ];
        // check for duplicate in the array built
        if (!this.checkDuplicate(tempRow)) {
          result = false;
          break;
        }
      }
    }
    return result;
  }

  /**
   * checkDuplicate common function to check duplicate in the array passed
   * @param {Array} row
   * @return {Boolean}
   */
  checkDuplicate(row) {
    let tempArr = [];
    let result = true;
    for (let i = 0; i < row.length; i++) {
      if (tempArr[row[i]]) {
        result = false;
        break;
      }
      tempArr[row[i]] = row[i];
    }
    return result;
  }
}

module.exports = SudokuCheckerService;
